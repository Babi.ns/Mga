import React from 'react';
import $ from "jquery";
import { Link } from 'react-router-dom'
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import './ajustes.css';

export default class CodAZ extends React.Component {
  render() {
    return (
      <div class="container">
      <table class="responsive-table">
        <thead>
          <tr>
              <th>A</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Another</td>
          </tr>
        </tbody>
      </table>
      <table class="responsive-table">
        <thead>
          <tr>
              <th>B</th>
          </tr>
        </thead>
        <td>Boku no pico</td>
        <tbody>
          <tr>

          </tr>
        </tbody>
      </table>
      <table class="responsive-table">
        <thead>
          <tr>
              <th>F</th>
          </tr>
        </thead>
        <td>Free</td>
        <tbody>
          <tr>

          </tr>
        </tbody>
      </table>
      <table class="responsive-table">
        <thead>
          <tr>
              <th>O</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>One Piece</td>
          </tr>
        </tbody>
      </table>
      <table class="responsive-table">
        <thead>
          <tr>
              <th>Y</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Yuri on ice</td>
          </tr>
        </tbody>
      </table>

    </div>
      );
    }
  }
