import React from 'react';
import $ from "jquery";
import { Link } from 'react-router-dom'
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import './ajustes.css';

export default class CabecalhoS extends React.Component {
  render() {
    return (
      <nav className="black hide-on-large-only">
          <div className="nav-wrapper">
            <Link className="brand-logo titsmall left" href="index.html" to="/">Mgá <img className="left responsive-img logosmall" src="./_img/logo2.png" /></Link>
        <div className="container hide-on-large-only">
          <SideNav
            trigger={<Button className="black right"><i className="material-icons">menu</i></Button>}
            options={{ closeOnClick: true }}
            >
            <nav className="grey darken-3 z-depth-0 ">
              <div className="nav-wrapper">
                <form>
                  <div className="input-field">
                    <input id="search" className="bordacolor2" type="search" required />
                    <label className="label-icon" for="search"><i className="material-icons">search</i></label>
                    <i className="material-icons">close</i>
                  </div>
                </form>
              </div>
            </nav>
            <ul className="collection">
              <li className="collection-item avatar clor">
                <svg className="circle" fill="#000000" height="40" viewBox="0 0 24 24" width="40">
                      <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 3c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 14.2c-2.5 0-4.71-1.28-6-3.22.03-1.99 4-3.08 6-3.08 1.99 0 5.97 1.09 6 3.08-1.29 1.94-3.5 3.22-6 3.22z"/>
                      <path d="M0 0h24v24H0z" fill="none"/>
                  </svg>
                <span className="title">Nick</span>
                <p>Nome <br />
                   Sobrenome
                </p>
              </li>
              <Link to="/Conta"><SideNavItem href='#'>Conta</SideNavItem></Link>
              <Link to="/Publicacoes"><SideNavItem href='#'>Publicações</SideNavItem></Link>
              <li><SideNavItem href='#'>Sair</SideNavItem></li>
            </ul>

            <Link to="/Indicacoes"><SideNavItem href='#'>Indicações</SideNavItem></Link>
            <Link to="/AZ"><SideNavItem href='#'>A-Z</SideNavItem></Link>
            <Link to="/Entrar"><SideNavItem href='#'>Entrar</SideNavItem></Link>

          </SideNav>

        </div>


      </div>
      </nav>
    );
  }
}
