import React from 'react';
import $ from "jquery";
import { Link } from 'react-router-dom'
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import './ajustes.css';


export default class CabecalhoML extends React.Component {
  render() {
    return (
<nav className="black">
<div className="nav-wrapper">

  <ul id="nav-mobile" className="left hide-on-med-and-down">
    <li><Link className="tit" to="/">Mgá <img className="left logo" src="_img/logo2.png" /></Link></li>
    <li><Link to="/Indicacoes">Indicações</Link></li>
    <li><Link to="/AZ">A-Z</Link></li>
  </ul>
  <ul id="nav-mobile" className="right hide-on-med-and-down">
    <li><Link to="/Entrar">Entrar</Link></li>
      <li>
        <nav className="grey darken-3 z-depth-0 ">
          <div className="nav-wrapper">
            <form>
              <div className="input-field">
                <input id="search" className="bordacolor2" type="search" required />
                <label className="label-icon" for="search"><i className="material-icons">search</i></label>
                <i className="material-icons">close</i>
              </div>
            </form>
          </div>
        </nav>
      </li>
  </ul>
</div>
</nav>
  );
 }
}
