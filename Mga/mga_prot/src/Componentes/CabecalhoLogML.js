import React from 'react';
import $ from "jquery";
import { Link } from 'react-router-dom'
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import './ajustes.css';


export default class CabecalhoML extends React.Component {
  render() {
    return (
      <nav className="black">
      <div className="nav-wrapper">

        <ul id="nav-mobile" className="left hide-on-med-and-down">
          <li><Link className="tit" to="/">Mgá <img className="left logo" src="_img/logo2.png" /></Link></li>
          <li><Link to="/Indicacoes">Indicações</Link></li>
          <li><Link to="/AZ">A-Z</Link></li>
        </ul>
        <ul id="nav-mobile" className="right hide-on-med-and-down">

            <li className="logado">
              <a className="dropdown-button left" href='#' data-activates='dropdown1'><svg fill="#FFFFFF" height="40" viewBox="0 0 24 24" width="40">
                  <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 3c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 14.2c-2.5 0-4.71-1.28-6-3.22.03-1.99 4-3.08 6-3.08 1.99 0 5.97 1.09 6 3.08-1.29 1.94-3.5 3.22-6 3.22z"/>
                  <path d="M0 0h24v24H0z" fill="none"/>
              </svg></a>
            </li>
            <li>
              <nav className="grey darken-3 z-depth-0 ">
                <div className="nav-wrapper">
                  <form>
                    <div className="input-field">
                      <input id="search" className="bordacolor2" type="search" required />
                      <label className="label-icon" for="search"><i className="material-icons">search</i></label>
                      <i className="material-icons">close</i>
                    </div>
                  </form>
                </div>
              </nav>
            </li>
        </ul>

         <ul id='dropdown1' className="ajustdropdow red darken-4">
            <li><Link to="/Conta">Conta</Link></li>
            <li><Link to="/Publicacoes" >Publicações</Link></li>
            <li><a href="#!">Sair</a></li>
         </ul>
       </div>
      </nav>
  );
 }
}
