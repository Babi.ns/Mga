import React from 'react';
import $ from "jquery";
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import Login from './Login';
import CabecalhoML from './CabecalhoLogML';
import CabecalhoS from './CabecalhoLogS';
import CodPubli from './CodPubli';

export default class Publicacoes extends React.Component {
  render() {
    return (
      <div>
        <CabecalhoML />
        <CabecalhoS />
        <CodPubli />
      </div>
    );
  }
}
