import React from 'react';
import $ from "jquery";
import { Link } from 'react-router-dom'
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import './ajustes.css';

export default class CabecalhoS extends React.Component {
  render() {
    return (
      <nav className="black hide-on-large-only">
          <div className="nav-wrapper">
            <Link className="brand-logo titsmall left" href="index.html" to="/">Mgá <img className="left responsive-img logosmall" src="./_img/logo2.png" /></Link>
        <div className="container hide-on-large-only">
          <SideNav
            trigger={<Button className="black right"><i className="material-icons">menu</i></Button>}
            options={{ closeOnClick: true }}
            >
            <nav className="grey darken-3 z-depth-0 ">
              <div className="nav-wrapper">
                <form>
                  <div className="input-field">
                    <input id="search" className="bordacolor2" type="search" required />
                    <label className="label-icon" for="search"><i className="material-icons">search</i></label>
                    <i className="material-icons">close</i>
                  </div>
                </form>
              </div>
            </nav>
            
            <Link to="/Indicacoes"><SideNavItem href='#'>Indicações</SideNavItem></Link>
            <Link to="/AZ"><SideNavItem href='#'>A-Z</SideNavItem></Link>
            <Link to="/Entrar"><SideNavItem href='#'>Entrar</SideNavItem></Link>

          </SideNav>

        </div>


      </div>
      </nav>
    );
  }
}
