import React from 'react';
import $ from "jquery";
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import './ajustes.css';

export default class Footer extends React.Component {
  render() {
    return(
      <footer className="page-footer red darken-4">
             <div className="container">
               <div className="row">
                 <div className="col l6 s12">
                   <h5 className="white-text">Footer Content</h5>
                 </div>
                 <div className="col l4 offset-l2 s12">
                   <h5 className="white-text">Links</h5>
                 </div>
               </div>
             </div>

             <div className="footer-copyright">
               <div className="container">
               © 2017 Copyright Text
               <a className="grey-text text-lighten-4 right" href="#!">More Links</a>
               </div>
             </div>
           </footer>
    );
  }
}
