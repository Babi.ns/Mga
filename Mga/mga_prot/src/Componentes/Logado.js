import React from 'react';
import $ from "jquery";
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import Login from './Login';
import CabecalhoML from './CabecalhoLogML';
import CabecalhoS from './CabecalhoLogS';
import Inicio from './CodInicio';

export default class Logado extends React.Component {
  render() {
    return (
      <div>
        <CabecalhoML />
        <CabecalhoS />
        <Inicio />
      </div>
    );
  }
}
