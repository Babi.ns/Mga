import React from 'react';
import $ from "jquery";
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import Login from './Login';
import CabecalhoML from './CabecalhoML';
import CabecalhoS from './CabecalhoS';
import CodIndicacoes from './CodIndicacoes';

export default class Indicacoes extends React.Component {
  render() {
    return (
      <div>
        <CabecalhoML />
        <CabecalhoS />
        <CodIndicacoes />
      </div>
    );
  }
}
