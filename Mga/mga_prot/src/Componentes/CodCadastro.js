import React from 'react';
import $ from "jquery";
import { Link } from 'react-router-dom'
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import './ajustes.css';

export default class Cadastro extends React.Component {
  render() {
    return (
        <div className="back">
          <form className="center fund" action="index.html" method="post">
            <div className="row container">

              <nav className="transparent z-depth-0">
                <div className="col s12 m6 l6 push-m3 push-l3">
                  <p className="center titgr">Cadastro</p>
                  <input className="bordacolor" type="text" name="usuario" placeholder="Usuario" />
                  <input className="bordacolor" type="text" name="nome" placeholder="Nome" />
                  <input className="bordacolor" type="text" name="email" placeholder="Email" />
                  <input className="bordacolor" type="text" name="senha" placeholder="Senha" />
                  <input className="bordacolor" type="text" name="confirmarsenha" placeholder="Confirmar Senha" />
                  <p className="dat">Data de Nascimento:</p>
                  <input className="bordacolor" type="date" name="nascimento" />
                  <input className="btn black hoverable waves-effect ajust" type="submit" name="cadastrar" value="Cadastrar" />
                </div>
             </nav>
            </div>
          </form>
        </div>
      );
    }
  }
