
import React from 'react';
import $ from "jquery";
import { Link } from 'react-router-dom'
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import './ajustes.css';

export default class Perfil extends React.Component {
  render() {
    return (
      <div className="container pru">
              <form action="index.html" method="post">
              <div className="row">
              <nav className="transparent z-depth-0">
                <div className="col s12 m6 l6 push-m3 push-l3">
                  <svg className="left ajustusuario" fill="#000000" height="100" viewBox="0 0 24 24" width="100">
                        <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 3c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 14.2c-2.5 0-4.71-1.28-6-3.22.03-1.99 4-3.08 6-3.08 1.99 0 5.97 1.09 6 3.08-1.29 1.94-3.5 3.22-6 3.22z"/>
                        <path d="M0 0h24v24H0z" fill="none"/>
                  </svg>
                  <p className="nao">Nick: Yoongato</p>
                  <p className="ajustconta">Nome: Mole Nobre</p>
                  <p className="ajustconta">Email: exemplo@exemplo.com</p>
                  <p className="ajustconta1">Data de Nascimento: 28/11/1997</p>
                  <p className="ajustconta2">Senha: *******</p>
                  <a className="waves-effect waves-light right ajusteditar"><i className="material-icons">create</i></a>
                </div>
             </nav>
            </div>
            </form>
        </div>
      );
    }
  }
