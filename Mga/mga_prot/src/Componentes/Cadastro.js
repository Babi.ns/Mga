import React from 'react';
import $ from "jquery";
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import Cadastro from './CodCadastro';
import CabecalhoML from './CabecalhoML';
import CabecalhoS from './CabecalhoS';

export default class Entrar extends React.Component {
  render() {
    return (
      <div>
        <CabecalhoML />
        <CabecalhoS />
        <Cadastro />
      </div>
    );
  }
}
