import React from 'react';
import $ from "jquery";
import { Link } from 'react-router-dom'
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import './ajustes.css';

export default class Login extends React.Component {
  render() {
    return (
      <div className="back">
      <form className="center fund" action="index.html" method="post">
        <div className="row container">

          <nav className="transparent z-depth-0">
            <div className="col s12 m6 l6 push-m3 push-l3">
              <p className="center titgr">Entrar</p>
              <p className="center flow-text aj">Não tem uma conta no Mgá? <Link className="ajs" to="/Cadastro"> Inscrever-se. </Link></p>
              <input className="input-field teste bordacolor" type="text" name="email" placeholder="Email" />
              <input className="input-field  bordacolor" type="text" name="senha" placeholder="Senha" />
              <Link to="/Logado"><input className="btn black hoverable waves-effect ajust" type="submit" name="entrar" value="Entrar" /></Link>
            </div>
          </nav>
        </div>
      </form>
      </div>
    );
  }
}
