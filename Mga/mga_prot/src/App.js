import React from 'react';
import $ from "jquery";
import {Button, Icon, SideNav, SideNavItem} from 'react-materialize'
import Inicio from './Componentes/CodInicio';
import CabecalhoML from './Componentes/CabecalhoML';
import CabecalhoS from './Componentes/CabecalhoS';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <CabecalhoML />
        <CabecalhoS />
        <Inicio />
      </div>
    );
  }
}
