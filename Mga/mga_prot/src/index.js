import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Entrar from './Componentes/Entrar';
import Cadastro from './Componentes/Cadastro';
import App from './App';
import Footer from './Componentes/Footer';
import Indicacoes from './Componentes/Indicacoes';
import AZ from './Componentes/AZ';
import Logado from './Componentes/Logado';
import Conta from './Componentes/Conta';
import Publicacoes from './Componentes/Publicacoes';
import { BrowserRouter, Route, Link } from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render((
  <BrowserRouter>
     <div>
       <Route exact={true} path='/' component={App} />
       <Route exact={true} path='/Entrar' component={Entrar} />
       <Route exact={true} path='/Cadastro' component={Cadastro} />
       <Route exact={true} path='/Indicacoes' component={Indicacoes} />
       <Route exact={true} path='/AZ' component={AZ} />
       <Route exact={true} path='/Logado' component={Logado} />
       <Route exact={true} path='/Conta' component={Conta} />
       <Route exact={true} path='/Publicacoes' component={Publicacoes} />

     </div>
   </BrowserRouter>
	), document.getElementById('root'));

ReactDOM.render(<Footer />, document.getElementById('foot'));
registerServiceWorker();
